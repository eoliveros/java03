package Operation;
/**
 * Here are declarated the attributes of the class Products
 * within its getters, setters and its constructor.
 * @author Emiliano Oliveros 
 *
 */
public class Productos {
	//Attributes
	String Nombre;
	String Categoria;
	int Precio;
	int Stock;
	public Productos(String nombre, String categoria, int precio, int stock) {
		
	
		super();
		Nombre = nombre;
		Categoria = categoria;
		Precio = precio;
		Stock = stock;
	}
	// Constructor
	public Productos(Productos a) {
		
		
		super();
		this.Nombre = a.Nombre;
		this.Categoria = a.Categoria;
		this.Precio = a.Precio;
		this.Stock = a.Stock;
	}
	public Productos() {
	
	}
	//Getters and setters
	public String getNombre() {
		return Nombre;
	}
	public void setNombre(String nombre) {
		Nombre = nombre;
	}
	public String getCategoria() {
		return Categoria;
	}
	public void setCategoria(String categoria) {
		Categoria = categoria;
	}
	public int getPrecio() {
		return Precio;
	}
	public void setPrecio(int precio) {
		Precio = precio;
	}
	public int getStock() {
		return Stock;
	}
	public void setStock(int stock) {
		Stock = stock;
	}
	
	

}
