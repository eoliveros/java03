package Operation;

import java.util.ArrayList;
/**
 * In this class are all the methods that aggregate, remove and sell the products to a list.
 * @author Emiliano Oliveros
 */
public class Movimientos {

	ArrayList<Movimientos> claseCursos = new ArrayList<Movimientos>();
	ArrayList<Movimientos> ventas = new ArrayList<Movimientos>();
	/**
	 * This method aggregate products to the list.
	 * @param listaProductos array list
	 * @param producto1 array list
	 */
	public static void agregar(ArrayList<Productos> listaProductos, Productos producto1) {
		listaProductos.add(producto1);

	}
	/**
	 * This method removes products from the list.
	 * @param listaProductos array list
	 * @param producto1 array list
	 */
	public static void remover(ArrayList<Productos> listaProductos, Productos producto1) {
		listaProductos.remove(producto1);

	}
	/**
	 * This method counts the total of categories of products
	 * @param listaProductos array list
	 * @return int number of the size of categories
	 */
	public static int total(ArrayList<Productos> listaProductos) {
		int a = listaProductos.size();
		listaProductos.size();
		return a;
	}
	/**
	 * This method aggregate a purchase to the list.
	 * @param listaProductos array list
	 * @param venta array list
	 * @param venta1 array list
	 */
	public static void ventas (ArrayList<Productos> listaProductos, ArrayList<Productos> venta, Productos venta1){
		
			Productos aux = new Productos (venta1);
			aux.setStock(venta1.getStock()-1);
			Movimientos.remover(listaProductos, venta1);
			venta1.setStock(venta1.getStock()-aux.getStock());
			Movimientos.agregar(venta, venta1);
			Movimientos.agregar(listaProductos, aux);
	}
		
}
