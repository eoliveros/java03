package Run;

import java.util.ArrayList;
import java.util.List;
import Operation.Movimientos;
import Operation.Productos;
/**
 * This is the main class in which the project is executed.
 * @author Emiliano Oliveros
 */
public class Run {
	public static ArrayList<Productos> listaProductos = new ArrayList<Productos>();
	public static ArrayList<Productos> ventas = new ArrayList<Productos>();

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		// The products are declarated
		Productos audifonos = new Productos("Audifonos", "Tecnología", 1500, 10);
		Productos laptop = new Productos("Laptop", "Tecnología", 900, 9);
		Productos tenis = new Productos("Tenis", "Deportes", 1000, 15);
		Productos balon = new Productos("Balon", "Deportes", 400, 20);
		//Products are added to the list and is showed the array of actual inventory
		Movimientos.agregar(listaProductos, audifonos);
		Movimientos.agregar(listaProductos, laptop);
		Movimientos.agregar(listaProductos, tenis);
		Movimientos.agregar(listaProductos, balon);
		System.out.println("\nInventario Actual");
		for (Productos aux : listaProductos) {
		System.out.println(aux.getNombre() + aux.getStock());
		}
		//Categories available
		Movimientos.remover(listaProductos, audifonos);
		System.out.println("\nCategorias disponibles: ");
		for (Productos aux : listaProductos) {
			System.out.println(aux.getNombre() + aux.getStock());
		}
		//Product list
		Movimientos.total(listaProductos);
		System.out.println(Movimientos.total(listaProductos));
		Movimientos.ventas(listaProductos, ventas, audifonos);
		System.out.println("\nLista de productos");
		for (Productos aux : listaProductos) {
			System.out.println(aux.getNombre() + aux.getStock());

		}
		//Purchases 
		System.out.println("\nVentas Realizadas:");
		for (Productos aux : ventas) {
			System.out.println(aux.getNombre() + aux.getStock());

		}

	}
}
